package ru.test.usercrud.dao;

import ru.test.usercrud.model.User;

import java.util.List;

/**
 * Created by Ruslan on 01.06.2017.
 */
public interface UserDao {
    public void addUser(User user);

    public void updateUser(User user);

    public void removeUser(int id);

    public User getUserById(int id);

    public List<User> listUsers();

    public List<User> getUsersByName(String name);
}
