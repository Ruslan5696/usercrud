package ru.test.usercrud.controller;

import ru.test.usercrud.model.User;
import ru.test.usercrud.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayDeque;
import java.util.List;
import java.util.Random;

/**
 * Created by Ruslan on 01.06.2017.
 */
@Controller
public class UserController {

    private UserService userService;
    private List<List<User>> pagedList;

    @Autowired(required = true)
    @Qualifier(value = "userService")
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String listUsers(Model model){
        pagedList = userService.splitListToPages(5, userService.listUsers());
        return listUsers(model, 0);
    }

    @RequestMapping(value = "/{page}", method = RequestMethod.GET)
    public String listUsers(Model model, @PathVariable("page") int page){
        model.addAttribute("user", new User());
        addPagedListToModel(model, page);
        return "users";
    }

    @RequestMapping(value = "/foundEntries", method = RequestMethod.POST)
    public String bookData(@RequestParam("findName") String name, Model model){
        pagedList = userService.splitListToPages(3, userService.getUsersByName(name));
        model.addAttribute("user", new User());
        addPagedListToModel(model, 0);
        return "users";
    }

    @RequestMapping(value = "/users/add", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("user") User user){
        if(user.getId() == 0){
            //user.setCreatedDate(new GregorianCalendar());
            this.userService.addUser(user);
        }else {
            user.setCreatedDate(userService.getUserById(user.getId()).getCreatedDate());
            this.userService.updateUser(user);
        }

        return "redirect:/";
    }

    @RequestMapping("/remove/{id}")
    public String removeUser(@PathVariable("id") int id){
        this.userService.removeUser(id);

        return "redirect:/";
    }

    @RequestMapping("edit/{id}")
    public String editUser(@PathVariable("id") int id, Model model){
        model.addAttribute("user", this.userService.getUserById(id));
        addPagedListToModel(model, 0);

        return "users";
    }

    @RequestMapping("/fillTable")
    public String fillTable(){
        for (int i = 0; i < 3; i++) {
            User user = new User();
            user.setAge(new Random(47).nextInt(100));
            user.setName("Name_" + i);
            user.setAdmin(new Random(12).nextBoolean());

            userService.addUser(user);
        }

        return "redirect:/";
    }

    private void addPagedListToModel(Model model, int page){
        if (pagedList.size()>0) {
            model.addAttribute("listUsers", pagedList.get(page));
            model.addAttribute("totalPages", pagedList.size());
        }
        else
            model.addAttribute("listUsers", new ArrayDeque<User>());
    }

}
