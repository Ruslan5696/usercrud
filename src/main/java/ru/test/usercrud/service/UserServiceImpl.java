package ru.test.usercrud.service;

import ru.test.usercrud.dao.UserDaoImpl;
import ru.test.usercrud.model.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ruslan on 01.06.2017.
 */
@Service
public class UserServiceImpl implements UserService {

    private UserDaoImpl userDao;

    @Override
    @Transactional
    public void addUser(User user) {
        userDao.addUser(user);
    }

    @Override
    @Transactional
    public void updateUser(User user) {
        userDao.updateUser(user);
    }

    @Override
    @Transactional
    public void removeUser(int id) {
        userDao.removeUser(id);
    }

    @Override
    @Transactional
    public User getUserById(int id) {
        return userDao.getUserById(id);
    }

    @Override
    @Transactional
    public List<User> listUsers() {
        return userDao.listUsers();
    }

    @Override
    public List<List<User>> splitListToPages(int elementsOnPage, List<User> userList) {
        List<List<User>> result = new ArrayList<>(userList.size() / elementsOnPage + (userList.size() % elementsOnPage == 0 ? 0 : 1));
        for (int i = 0; i < userList.size() / elementsOnPage + (userList.size() % elementsOnPage == 0 ? 0 : 1); i++) {
            result.add(new ArrayList<User>());
            for (int j = 0; j < (elementsOnPage < userList.size() - elementsOnPage * i ? elementsOnPage : userList.size() - elementsOnPage * i); j++) {
                result.get(i).add(userList.get(elementsOnPage * i + j));
            }
        }
        return result;
    }

    @Override
    @Transactional
    public List<User> getUsersByName(String name) {
        return userDao.getUsersByName(name);
    }

    public void setUserDao(UserDaoImpl userDao) {
        this.userDao = userDao;
    }
}
